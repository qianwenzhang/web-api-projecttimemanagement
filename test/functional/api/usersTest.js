import chai from 'chai'
const expect = chai.expect
const _ = require("lodash")
const dotenv = require("dotenv")
const request = require("supertest")
dotenv.config()

let server = require("../../../bin/www")

describe("Users",  () => {
  describe("GET /addFriend/:id", () => {
    describe("when there are related results", () => {
      it("should return the matching user", done => {
        request(server)
          .get("/addFriend/123t")
          .set("Accept", "application/json")
          .expect("Content-Type", /json/)
          .expect(200)
          .expect({message: "User has found! Do you want to send a request?"})
          .end((err, res) => {
            try {
              expect(res.body[0]).to.include({userEmail: "123@gmail.test.com"})
              done()
            } catch (e) {
              done(e)
            }
          })
      })
    })
    describe("when no results are related", () => {
      it("should return the NOT found message", done => {
        request(server)
          .get("/addFriend/9999")
          .set("Accept", "application/json")
          .expect("Content-Type", /json/)
          .expect(200)
          .end((err, res) => {
            expect(res.body.message).equals("USER NOT FOUND")
            done(err)
          })
      })
    })
  })

  describe("POST /userCreate", () => {
    describe("if the input userEmail is the only one", () =>{
      it("should return confirmation and update database ", () => {
        const user = {
          userName: "Super Mario",
          userPassword: "MarioJumpsHigh",
          userEmail: "Marioooooooo123@gmail.com",
          userCoins: 10000
        }
        return request(server)
          .post("/userCreate")
          .send(user)
          .expect(200)
          .then(res => {
            expect({message: "User has registered successfully"})
          })
      })
    })

    describe("if the email has already be registerd", () => {
      it("should return the error message", () => {
        const user = {
          userName: "Super Mario",
          userEmail: "Marioooooooo123@gmail.com"
        }

        return request(server)
          .post("/userCreate")
          .send(user)
          .expect(200)
          .then(res => {
            expect({message: "user email already exists"})
          })
      })
    })
  })
  // describe("GET /getCoinBalance/:id", () => {
    // describe("get the coins when id is valid ",() =>{
    //   it("should return the coins that user has", done =>{
    //     request(server)
    //       .get("/getCoinBalance/5db5f1276df19224807d71db")
    //       .set("Accept", "application/json")
    //       .expect("Content-Type", /json/)
    //       .expect(200)
    //       .end((err,res) => {
    //         expect(res.body.message).equals("your coins: { userCoins: 98000 }")
    //         done(err)
    //       })
    //   })
    // })
    // describe("return the information if id is invalid", () => {
    //   it("return the error", done => {
    //     request(server)
    //       .get("/getCoinBalance/1444444")
    //       .set("Accept", "application/json")
    //       .expect("Content-Type", /json/)
    //       .expect(200)
    //       .end((err,res) => {
    //         expect(res.body.message).equals("USER NOT Found!")
    //         done(err)
    //       })
    //   })
    // })
  // })

  describe("GET /getUser/:id", () => {
    describe("get the user if id is valid ",() =>{
      it("should return the information of specific user", done =>{
        request(server)
          .get("/getUser/5db5f1276df19224807d71db")
          .set("Accept", "application/json")
          .expect("Content-Type", /json/)
          .expect(200)
          .end((err,res) => {
            let result = _.map(res.body, users => {
              return {userEmail: users.userEmail}
            })
            //       expect(result[0]).to.include({userEmail: "james@gmail.com"});
            done()
          })
      })
    })

  })
})
