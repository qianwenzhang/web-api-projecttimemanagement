import chai from 'chai'
const expect = chai.expect
// const _ = require("lodash")
const dotenv = require("dotenv")
const request = require("supertest")
dotenv.config()

let server = require("../../../bin/www")

describe("Tags",  () => {
  describe("PUT /tagEdition/:id", () => {
    describe("when the id is valid during editing", () => {
      it("should return a message and database updates", () => {
        const tag = {
          tagColor: "pink"
        }
        return request(server)
          .put("/tagEdition/5db633dc1666192d90185683")
          .send(tag)
          .expect(200)
          .then(res => {
            // expect(res.body).to.include({
            //     message: "Tag edited!"
            // })
            expect(res.body.data).to.have.property("tagColor", "pink")
          })
      })
    })
    describe("when the id is invalid while editing", () => {
      it("should return the message ", () => {
        const tag = {
          tagColor: "pink"
        }
        return request(server)
          .put("/tagEdition/5db6")
          .send(tag)
          .expect(200)
          .then(res => {
            expect(res.body.message).equals("Cast to ObjectId failed for value \"5db6\" at path \"_id\" for model \"Tag\"")
          })
      })
    })

  })

  describe("POST /tagCreation", () => {
    describe("when user creates an tag", () => {
      it("should return message about creation is successful or not ", () => {
        const tag = {
          tagType: "Socialize",
          tagColor: "yellow",
          tagDescription: "spend time with families"
        }
        return request(server)
          .post("/tagCreation")
          .send(tag)
          .expect(200)
          .then(res => {
            expect(res.body.message).equal("Tag has already existed!" )
          })
      })
    })
  })
  // describe("DELETE /deleteTag/:id",() => {
  //     describe("when the id is valid",() => {
  //         it("should return the successful message with valid input when deleting TAG", () => {
  //             return request(server)
  //                 .delete("/deleteTag/5dbfcb56a6de3956b848575e")
  //                 .expect({message: "Tag Deleted!"})
  //
  //         })
  //
  //     })
  //     describe("when the tag id is invalid",() => {
  //         it("should return message with invalid input when deleting TAG", () => {
  //             return request(server)
  //                 .delete("/deleteTag/1000")
  //                 .expect(200)
  //                 .expect({message: "Tag NOT Deleted!"})
  //         })
  //     })
  // })
  describe("GET /getTags", () => {
    it("should return all the tags", done => {
      request(server)
        .get("/getTags")
        .set("Accept", "application/json")
        .expect("Content-Type", /json/)
        .end((err, res) => {
          try {
            expect(res.body).to.be.a("array")
            done()
          } catch (e) {
            done(e)
          }
        })
    })
  })

  describe("GET get tag by id /getTag/:id", () => {
    describe("if the id is valid", () => {
      it("shoule return the tag information", done => {
        request(server)
          .get("/getTag/5df34e13dc28385a20604a6d")
          .set("Accept", "application/json")
          .expect("Content-Type", /json/)
          .end((err, res) => {
            try {
              expect(res.body[0]).to.have.property("tagType", "Test")
              done()
            } catch (e) {
              done(e)
            }
          })
      })
    })
    describe("if the id is invalid", () => {
      it("should return the wrong information", done => {
        request(server)
          .get("/getTag/5d20604a6d")
          .set("Accept", "application/json")
          .expect("Content-Type", /json/)
          .end((err, res) => {
            expect(res.body.message).equals("TAG NOT Found!")
            done(err)
          })
      })
    })

  })
})
