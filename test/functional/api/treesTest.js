import chai from 'chai'
const expect = chai.expect
// const _ = require("lodash")
const dotenv = require("dotenv")
const request = require("supertest")
dotenv.config()

let server = require("../../../bin/www")
describe("Trees", ()=> {


  describe("GET /plantList",  () => {
    it("should return all the plants in store", done => {
      request(server)
        .get("/plantList")
        .set("Accept", "application/json")
        .expect("Content-Type", /json/)
        .end((err, res) => {
          try {
            expect(res.body).to.be.a("array")
            done()
          } catch(e){
            done(e)
          }
        })
    })
  })
})
